#!/bin/bash

export DOLPHINSCHEDULER_HOME=${DOLPHINSCHEDULER_HOME:-"/opt/cloudera/parcels/DOLPHINSCHEDULER-1.3.9-1.cdh6.3.2/lib/dolphinscheduler"}
echo "DOLPHINSCHEDULER_HOME: ${DOLPHINSCHEDULER_HOME}"

DS_LOG_PATH=(`cat common-conf.properties | grep 'ds.log.dir=' |sed 's/^[^=]*=//'`)
export DOLPHINSCHEDULER_LOG_PATH=${DS_LOG_PATH}
echo "DOLPHINSCHEDULER_LOG_PATH: ${DS_LOG_PATH}"

export DOLPHINSCHEDULER_CONF_PATH=${CONF_DIR}/ds-conf
if [ ! -d "${DOLPHINSCHEDULER_CONF_PATH}" ];then
	mkdir ${DOLPHINSCHEDULER_CONF_PATH}
else
	rm -rf ${DOLPHINSCHEDULER_CONF_PATH}/*
fi

echo "DOLPHINSCHEDULER_CONF_PATH: ${DOLPHINSCHEDULER_CONF_PATH}"
	
export DOLPHINSCHEDULER_PID_PATH=${DOLPHINSCHEDULER_CONF_PATH}
echo "DOLPHINSCHEDULER_PID_PATH: ${DOLPHINSCHEDULER_PID_PATH}"
	
init_config() {
    txt=""
    if [[ "${OSTYPE}" == "darwin"* ]]; then
        # Mac OSX
        txt="''"
    fi

	MAX_BACKUP_INDEX=10
	MAX_FILE_SIZE=200MB
	if [ "${SERVER_NAME}" == "api-server" ];then
		API_HEAP_OPTS="-Xms1g -Xmx1g -Xmn512m"
		TMP_MAX_HEAP_SIZE=(`cat api-conf.properties | grep 'api.server.max.heap.size=' |sed 's/^[^=]*=//'`)
		API_MAX_HEAP_SIZE=$((TMP_MAX_HEAP_SIZE/1))
		if [ ${API_MAX_HEAP_SIZE} > 1073741824 ];then
			MAX_HEAP_SIZE=$((API_MAX_HEAP_SIZE/1024/1024/1024))
			API_HEAP_OPTS="-Xms1g -Xmx${MAX_HEAP_SIZE}g -Xmn512m"
			echo "API_MAX_HEAP_SIZE: ${MAX_HEAP_SIZE}g"
		fi
		export API_HEAP_OPTS
		MAX_BACKUP_INDEX=(`cat log4j-api.properties | grep 'max.log.file.backup.index=' | sed 's/^[^=]*=//'`)
		MAX_FILE_SIZE=(`cat log4j-api.properties | grep 'max.log.file.size=' |sed 's/^[^=]*=//'`)
	fi

	if [ "${SERVER_NAME}" == "master-server" ];then
		MASTER_HEAP_OPTS="-Xms1g -Xmx4g -Xmn512m"
		TMP_MAX_HEAP_SIZE=(`cat master-conf.properties | grep 'master.server.max.heap.size=' |sed 's/^[^=]*=//'`)
		MASTER_MAX_HEAP_SIZE=$((TMP_MAX_HEAP_SIZE/1))
		if [ ${MASTER_MAX_HEAP_SIZE} > 1073741824 ];then
			MAX_HEAP_SIZE=$((MASTER_MAX_HEAP_SIZE/1024/1024/1024))
			MASTER_HEAP_OPTS="-Xms1g -Xmx${MAX_HEAP_SIZE}g -Xmn512m"
			echo "MASTER_MAX_HEAP_SIZE: ${MAX_HEAP_SIZE}g"
		fi
		export MASTER_HEAP_OPTS
		MAX_BACKUP_INDEX=(`cat log4j-master.properties | grep 'max.log.file.backup.index=' | sed 's/^[^=]*=//'`)
		MAX_FILE_SIZE=(`cat log4j-master.properties | grep 'max.log.file.size=' |sed 's/^[^=]*=//'`)
	fi

	if [ "${SERVER_NAME}" == "worker-server" ];then
		WORKER_HEAP_OPTS="-Xms1g -Xmx2g -Xmn512m"
		TMP_MAX_HEAP_SIZE=(`cat worker-conf.properties | grep 'worker.server.max.heap.size=' |sed 's/^[^=]*=//'`)
		WORKER_MAX_HEAP_SIZE=$((TMP_MAX_HEAP_SIZE/1))
		if [ ${WORKER_MAX_HEAP_SIZE} > 1073741824 ];then
			MAX_HEAP_SIZE=$((WORKER_MAX_HEAP_SIZE/1024/1024/1024))
			WORKER_HEAP_OPTS="-Xms1g -Xmx${MAX_HEAP_SIZE}g -Xmn512m"
			echo "WORKER_MAX_HEAP_SIZE: ${MAX_HEAP_SIZE}g"
		fi
		export WORKER_HEAP_OPTS
		MAX_BACKUP_INDEX=(`cat log4j-worker.properties | grep 'max.log.file.backup.index=' |sed 's/^[^=]*=//'`)
		MAX_FILE_SIZE=(`cat log4j-worker.properties | grep 'max.log.file.size=' |sed 's/^[^=]*=//'`)
	fi

	if [ "${SERVER_NAME}" == "logger-server" ];then
		LOGGER_HEAP_OPTS="-Xms1g -Xmx1g -Xmn512m"
		TMP_MAX_HEAP_SIZE=(`cat logger-conf.properties | grep 'logger.server.max.heap.size=' |sed 's/^[^=]*=//'`)
		LOGGER_MAX_HEAP_SIZE=$((TMP_MAX_HEAP_SIZE/1))
		if [ ${LOGGER_MAX_HEAP_SIZE} > 1073741824 ];then
			MAX_HEAP_SIZE=$((LOGGER_MAX_HEAP_SIZE/1024/1024/1024))
			LOGGER_HEAP_OPTS="-Xms1g -Xmx${MAX_HEAP_SIZE}g -Xmn512m"
			echo "LOGGER_MAX_HEAP_SIZE: ${MAX_HEAP_SIZE}g"
		fi
		export LOGGER_HEAP_OPTS
		MAX_BACKUP_INDEX=(`cat log4j-logger.properties | grep 'max.log.file.backup.index=' |sed 's/^[^=]*=//'`)
		MAX_FILE_SIZE=(`cat log4j-logger.properties | grep 'max.log.file.size=' |sed 's/^[^=]*=//'`)
	fi

	if [ "${SERVER_NAME}" == "alert-server" ];then
		ALERT_HEAP_OPTS="-Xms1g -Xmx1g -Xmn512m"
		TMP_MAX_HEAP_SIZE=(`cat alert-conf.properties | grep 'alert.server.max.heap.size=' |sed 's/^[^=]*=//'`)
		ALERT_MAX_HEAP_SIZE=$((TMP_MAX_HEAP_SIZE/1))
		if [ ${ALERT_MAX_HEAP_SIZE} > 1073741824 ];then
			MAX_HEAP_SIZE=$((ALERT_MAX_HEAP_SIZE/1024/1024/1024))
			ALERT_HEAP_OPTS="-Xms1g -Xmx${MAX_HEAP_SIZE}g -Xmn512m"
			echo "ALERT_MAX_HEAP_SIZE: ${MAX_HEAP_SIZE}g"
		fi
		export ALERT_HEAP_OPTS
		MAX_BACKUP_INDEX=(`cat log4j-alert.properties | grep 'max.log.file.backup.index=' |sed 's/^[^=]*=//'`)
		MAX_FILE_SIZE=(`cat log4j-alert.properties | grep 'max.log.file.size=' |sed 's/^[^=]*=//'`)
	fi
	
	cp -r ${DOLPHINSCHEDULER_HOME}/conf/config ${DOLPHINSCHEDULER_CONF_PATH}/config
	cp -r ${DOLPHINSCHEDULER_HOME}/conf/i18n ${DOLPHINSCHEDULER_CONF_PATH}/i18n
	cp -r ${DOLPHINSCHEDULER_HOME}/conf/env ${DOLPHINSCHEDULER_CONF_PATH}/env
	cp -r ${DOLPHINSCHEDULER_HOME}/conf/logback-alert.xml ${DOLPHINSCHEDULER_CONF_PATH}/logback-alert.xml
	cp -r ${DOLPHINSCHEDULER_HOME}/conf/logback-worker.xml ${DOLPHINSCHEDULER_CONF_PATH}/logback-worker.xml
	cp -r ${DOLPHINSCHEDULER_HOME}/conf/logback-master.xml ${DOLPHINSCHEDULER_CONF_PATH}/logback-master.xml
	cp -r ${DOLPHINSCHEDULER_HOME}/conf/logback-logger.xml ${DOLPHINSCHEDULER_CONF_PATH}/logback-logger.xml
	cp -r ${DOLPHINSCHEDULER_HOME}/conf/logback-api.xml ${DOLPHINSCHEDULER_CONF_PATH}/logback-api.xml
	cp -r ${CONF_DIR}/common-conf.properties ${DOLPHINSCHEDULER_CONF_PATH}/common.properties
	cp -r ${CONF_DIR}/zookeeper-conf.properties ${DOLPHINSCHEDULER_CONF_PATH}/zookeeper.properties
	cp -r ${CONF_DIR}/datasource-conf.properties ${DOLPHINSCHEDULER_CONF_PATH}/datasource.properties
	cp -r ${CONF_DIR}/quartz-conf.properties ${DOLPHINSCHEDULER_CONF_PATH}/quartz.properties
	cp -r ${CONF_DIR}/hadoop-conf/core-site.xml ${DOLPHINSCHEDULER_CONF_PATH}/core-site.xml
	cp -r ${CONF_DIR}/hadoop-conf/hdfs-site.xml ${DOLPHINSCHEDULER_CONF_PATH}/hdfs-site.xml
   
    STORAGE_TYPE=(`cat common-conf.properties | grep 'resource.storage.type=' |sed 's/^[^=]*=//'`)
    if [ "${STORAGE_TYPE}" == "HDFS" ];then
       HDFS_BASE_URL=(`cat ${CONF_DIR}/hadoop-conf/core-site.xml | grep 'hdfs://' | grep -oP '(?<=<value>).*(?=</value>)'`)
    fi
   
    DOLPHINSCHEDULER_ENV_FILE=${DOLPHINSCHEDULER_CONF_PATH}/env/dolphinscheduler_env.sh
   
    while IFS= read -r line;
    do 
        TMP_STR="${line//export/}"
	    TMP_ENV=(`eval echo "${TMP_STR}"`)
	    TMP_KEY="${TMP_ENV%=*}"
	    TMP_VAL="${TMP_ENV#*=}"
	    if [ -f "${DOLPHINSCHEDULER_ENV_FILE}" ]; then
		    if [ "${TMP_KEY^^}" == "HADOOP_HOME" ]; then
		        echo "replace \"export ${TMP_KEY}=${TMP_VAL}\""
			    sed -i ${txt} "s#export.*HADOOP_HOME=.*#export HADOOP_HOME=${TMP_VAL}#g" ${DOLPHINSCHEDULER_ENV_FILE}
		    elif [ "${TMP_KEY^^}" == "HADOOP_CONF_DIR" ]; then
		        echo "replace \"export ${TMP_KEY}=${TMP_VAL}\""
			    sed -i ${txt} "s#export.*HADOOP_CONF_DIR=.*#export HADOOP_CONF_DIR=${TMP_VAL}#g" ${DOLPHINSCHEDULER_ENV_FILE}
		    elif [ "${TMP_KEY^^}" == "SPARK_HOME1" ]; then
		        echo "replace \"export ${TMP_KEY}=${TMP_VAL}\""
			    sed -i ${txt} "s#export.*SPARK_HOME1=.*#export SPARK_HOME1=${TMP_VAL}#g" ${DOLPHINSCHEDULER_ENV_FILE}
		    elif [ "${TMP_KEY^^}" == "SPARK_HOME2" ]; then
		        echo "replace \"export ${TMP_KEY}=${TMP_VAL}\""
			    sed -i ${txt} "s#export.*SPARK_HOME2=.*#export SPARK_HOME2=${TMP_VAL}#g" ${DOLPHINSCHEDULER_ENV_FILE}
		    elif [ "${TMP_KEY^^}" == "HIVE_HOME" ]; then
		        echo "replace \"export ${TMP_KEY}=${TMP_VAL}\""
			    sed -i ${txt} "s#export.*HIVE_HOME=.*#export HIVE_HOME=${TMP_VAL}#g" ${DOLPHINSCHEDULER_ENV_FILE}
		    elif [ "${TMP_KEY^^}" == "PYTHON_HOME" ]; then
		        echo "replace \"export ${TMP_KEY}=${TMP_VAL}\""
			    sed -i ${txt} "s#export.*PYTHON_HOME=.*#export PYTHON_HOME=${TMP_VAL}#g" ${DOLPHINSCHEDULER_ENV_FILE}
		    elif [ "${TMP_KEY^^}" == "JAVA_HOME" ]; then
		        echo "replace \"export ${TMP_KEY}=${TMP_VAL}\""
			    sed -i ${txt} "s#export.*JAVA_HOME=.*#export JAVA_HOME=${TMP_VAL}#g" ${DOLPHINSCHEDULER_ENV_FILE}
		    elif [ "${TMP_KEY^^}" == "FLINK_HOME" ]; then
		        echo "replace \"export ${TMP_KEY}=${TMP_VAL}\""
			    sed -i ${txt} "s#export.*FLINK_HOME=.*#export FLINK_HOME=${TMP_VAL}#g" ${DOLPHINSCHEDULER_ENV_FILE}
		    elif [ "${TMP_KEY^^}" == "DATAX_HOME" ]; then
		        echo "replace \"export ${TMP_KEY}=${TMP_VAL}\""
			    sed -i ${txt} "s#export.*DATAX_HOME=.*#export DATAX_HOME=${TMP_VAL}#g" ${DOLPHINSCHEDULER_ENV_FILE}
		    elif [ "${TMP_KEY^^}" == "PATH" ]; then
		        echo "replace \"export ${TMP_KEY}=${TMP_VAL}\""
			    sed -i ${txt} "s#export.*PATH=.*#export PATH=${TMP_VAL}#g" ${DOLPHINSCHEDULER_ENV_FILE}
		    else
			    if [ "${TMP_VAL}" != "" ]; then
			        echo "append \"export ${TMP_KEY}=${TMP_VAL}\""
				    echo "export ${TMP_KEY}=${TMP_VAL}" >> ${DOLPHINSCHEDULER_ENV_FILE}
			    fi
		    fi
	    else
		    if [ "${TMP_VAL}" != "" ]; then
		        echo "write in \"export ${TMP_KEY}=${TMP_VAL}\""
			    echo "export ${TMP_KEY}=${TMP_VAL}" >> ${DOLPHINSCHEDULER_ENV_FILE}
		    fi
	    fi
    done < dolphinscheduler_env.sh
   
    chmod u+x ${DOLPHINSCHEDULER_ENV_FILE}
   
    echo "dolphinscheduler.env.path=${DOLPHINSCHEDULER_ENV_FILE}" >> ${DOLPHINSCHEDULER_CONF_PATH}/common.properties 
   
    echo "zookeeper.quorum=${ZK_QUORUM}" >> ${DOLPHINSCHEDULER_CONF_PATH}/zookeeper.properties
	
	sed -i ${txt} "s#spring.datasource.password=.*#spring.datasource.password=******#g" ${CONF_DIR}/datasource-conf.properties
   
    if [ "${SERVER_NAME}" == "api-server" ];then
	    cp -r ${CONF_DIR}/api-conf.properties ${DOLPHINSCHEDULER_CONF_PATH}/application-api.properties
        TMP_MAX_FILE_SIZE=(`cat api-conf.properties | grep 'spring.servlet.multipart.max-file-size=' |sed 's/^[^=]*=//'`)
	    TMP_MAX_REQU_SIZE=(`cat api-conf.properties | grep 'spring.servlet.multipart.max-request-size=' |sed 's/^[^=]*=//'`)
        sed -i ${txt} "s#spring.servlet.multipart.max-file-size=.*#spring.servlet.multipart.max-file-size=${TMP_MAX_FILE_SIZE}MB#g" ${DOLPHINSCHEDULER_CONF_PATH}/application-api.properties
        sed -i ${txt} "s#spring.servlet.multipart.max-request-size=.*#spring.servlet.multipart.max-request-size=${TMP_MAX_REQU_SIZE}MB#g" ${DOLPHINSCHEDULER_CONF_PATH}/application-api.properties
	    sed -i ${txt} "s#dslogs#${DOLPHINSCHEDULER_LOG_PATH}#g" ${DOLPHINSCHEDULER_CONF_PATH}/logback-api.xml
	    sed -i ${txt} "s#<maxIndex>.*</maxIndex>#<maxIndex>${MAX_BACKUP_INDEX}</maxIndex>#g" ${DOLPHINSCHEDULER_CONF_PATH}/logback-api.xml
        sed -i ${txt} "s#<maxFileSize>.*</maxFileSize>#<maxFileSize>${MAX_FILE_SIZE}</maxFileSize>#g" ${DOLPHINSCHEDULER_CONF_PATH}/logback-api.xml
    fi
   
    if [ "${SERVER_NAME}" == "master-server" ];then
	    cp -r ${CONF_DIR}/master-conf.properties ${DOLPHINSCHEDULER_CONF_PATH}/master.properties
        sed -i ${txt} "s#dslogs#${DOLPHINSCHEDULER_LOG_PATH}#g" ${DOLPHINSCHEDULER_CONF_PATH}/logback-master.xml
        sed -i ${txt} "s#<maxIndex>.*</maxIndex>#<maxIndex>${MAX_BACKUP_INDEX}</maxIndex>#g" ${DOLPHINSCHEDULER_CONF_PATH}/logback-master.xml
        sed -i ${txt} "s#<maxFileSize>.*</maxFileSize>#<maxFileSize>${MAX_FILE_SIZE}</maxFileSize>#g" ${DOLPHINSCHEDULER_CONF_PATH}/logback-master.xml
    fi
   
    if [ "${SERVER_NAME}" == "worker-server" ];then
	    cp -r ${CONF_DIR}/worker-conf.properties ${DOLPHINSCHEDULER_CONF_PATH}/worker.properties
        sed -i ${txt} "s#dslogs#${DOLPHINSCHEDULER_LOG_PATH}#g" ${DOLPHINSCHEDULER_CONF_PATH}/logback-worker.xml
        sed -i ${txt} "s#<maxIndex>.*</maxIndex>#<maxIndex>${MAX_BACKUP_INDEX}</maxIndex>#g" ${DOLPHINSCHEDULER_CONF_PATH}/logback-worker.xml
        sed -i ${txt} "s#<maxFileSize>.*</maxFileSize>#<maxFileSize>${MAX_FILE_SIZE}</maxFileSize>#g" ${DOLPHINSCHEDULER_CONF_PATH}/logback-worker.xml
    fi
   
    if [ "${SERVER_NAME}" == "logger-server" ];then
	    sed -i ${txt} "s#dslogs#${DOLPHINSCHEDULER_LOG_PATH}#g" ${DOLPHINSCHEDULER_CONF_PATH}/logback-logger.xml
	    sed -i ${txt} "s#<maxIndex>.*</maxIndex>#<maxIndex>${MAX_BACKUP_INDEX}</maxIndex>#g" ${DOLPHINSCHEDULER_CONF_PATH}/logback-logger.xml
        sed -i ${txt} "s#<maxFileSize>.*</maxFileSize>#<maxFileSize>${MAX_FILE_SIZE}</maxFileSize>#g" ${DOLPHINSCHEDULER_CONF_PATH}/logback-logger.xml
    fi
   
    if [ "${SERVER_NAME}" == "alert-server" ];then
	    cp -r ${CONF_DIR}/alert-conf.properties ${DOLPHINSCHEDULER_CONF_PATH}/alert.properties	   
	    sed -i ${txt} "s#dslogs#${DOLPHINSCHEDULER_LOG_PATH}#g" ${DOLPHINSCHEDULER_CONF_PATH}/logback-alert.xml
        sed -i ${txt} "s#<maxIndex>.*</maxIndex>#<maxIndex>${MAX_BACKUP_INDEX}</maxIndex>#g" ${DOLPHINSCHEDULER_CONF_PATH}/logback-alert.xml
        sed -i ${txt} "s#<maxFileSize>.*</maxFileSize>#<maxFileSize>${MAX_FILE_SIZE}</maxFileSize>#g" ${DOLPHINSCHEDULER_CONF_PATH}/logback-alert.xml
    fi
}

start_ds() {
    echo "Start Dolphinscheduler ${SERVER_NAME}"
    init_config
	export DOLPHINSCHEDULER_PID_PATH=${DOLPHINSCHEDULER_CONF_PATH} && export DOLPHINSCHEDULER_LOG_PATH=${DS_LOG_PATH} && exec ${DOLPHINSCHEDULER_HOME}/bin/dolphinscheduler-daemon-cdh.sh start ${SERVER_NAME}
}
 
stop_ds() {
    echo "Stop Dolphinscheduler ${SERVER_NAME}"
	export DOLPHINSCHEDULER_PID_PATH=${DOLPHINSCHEDULER_CONF_PATH} && export DOLPHINSCHEDULER_LOG_PATH=${DS_LOG_PATH} && exec ${DOLPHINSCHEDULER_HOME}/bin/dolphinscheduler-daemon-cdh.sh stop ${SERVER_NAME}
}

case "$1" in
    start)
        start_ds
        ;;
    stop)
        stop_ds
        ;;
    restart)
        stop_ds
        start_ds
        ;;
    *)
        echo "Usage dolphinscheduler {start|stop|restart}"
        exit 2
        ;;
esac