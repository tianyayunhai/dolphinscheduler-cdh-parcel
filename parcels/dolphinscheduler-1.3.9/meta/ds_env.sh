#!/bin/bash
CDH_PARCELS_ROOT=${PARCELS_ROOT:-"/opt/cloudera/parcels"}
CDH_DS_DIRNAME=${PARCEL_DIRNAME:-"DOLPHINSCHEDULER-1.3.9-1.cdh6.3.2"}
export DOLPHINSCHEDULER_HOME=${CDH_PARCELS_ROOT}/${CDH_DS_DIRNAME}/lib/dolphinscheduler
echo "DOLPHINSCHEDULER_HOME:"${DOLPHINSCHEDULER_HOME}
JAVA_PATH="/usr/java/default"
if [ ! -L "${JAVA_PATH}" ];then
    ln -s ${JAVA_HOME} ${JAVA_PATH}
	echo "JAVA_HOME:${JAVA_HOME}"
fi
CDH_TMP_DS_PATH="/tmp/dolphinscheduler"
if [ -d "${CDH_TMP_DS_PATH}" ];then
    chown dolphinscheduler:dolphinscheduler -R ${CDH_TMP_DS_PATH}
fi